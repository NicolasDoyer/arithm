module EvalSpec (main, spec) where

import Test.Hspec
import Eval
import Expr

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "eval" $ do
        it "1" $ eval (Val 42) `shouldBe` 42