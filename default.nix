{ pkgs ? import <nixpkgs> {} }:
let drv = pkgs.haskellPackages.callCabal2nix "arithm" ./. {};
in if pkgs.lib.inNixShell then drv.env else drv