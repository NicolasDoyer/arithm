-- |
-- Module Evel
-- This is a super module qui évalue une Expr
module Eval where

import Expr

-- |
-- Méthode qui évalue une Expr
eval :: Expr -> Int
eval (Val x) = x
eval (Add e1 e2) = eval e1 + eval e2
eval (Mul e1 e2) = eval e1 * eval e2

